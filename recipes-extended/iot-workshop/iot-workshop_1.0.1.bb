
SUMMARY = "The IoT Workshop stuff"
DESCRIPTION = "The IoT Workshop samples and labs"
HOMEPAGE = "http://www.windriver.com"
LICENSE = "GPLv2"
SECTION = "devel"

LIC_FILES_CHKSUM = \
"file://include/wra.h;md5=9c5ada73352c009769b1af85434542c8"

PR = "r0"

SRC_URI = "http://www.example.com/downloads/iot-workshop_1.0.1.tgz \
	file://rpm-2016-04-26.zip \
	file://gw_rpm_pgp_public_key \
	file://setup.sh"

SRC_URI[md5sum] = "17802e6f1a4737e974b168363169c837"
SRC_URI[sha256sum] = "9806f63f1df18edd1af44621f004c52a0ef155284e42f4cc4070857b138ce77c"

UBROKER_DIR =  "/opt/intel/ubroker/bin"

FILES_${PN} = "${UBROKER_DIR}/startup.bin.*"
FILES_${PN}+= "/root/iot-workshop/*"
FILES_${PN}+= "/root/rpm/*"
FILES_${PN}+= "/root/setup.sh"
FILES_${PN}+= "/etc/gw_rpm_pgp_public_key"

do_install() {
	install -d ${D}${UBROKER_DIR}
	install -m 0644 ${S}/startup/startup.bin.* ${D}${UBROKER_DIR}
	install -d ${D}/root/iot-workshop
	install -d ${D}/etc
	cp -a ${S}/* ${D}/root/iot-workshop
	cp -a ${S}/../rpm ${D}/root
	cp -a ${S}/../gw_rpm_pgp_public_key ${D}/etc
	cp -a ${S}/../setup.sh ${D}/root
}

INSANE_SKIP_${PN} = "dev-so already-stripped"
