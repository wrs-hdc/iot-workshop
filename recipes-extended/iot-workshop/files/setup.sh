#!/bin/bash

rpm --import /etc/gw_rpm_pgp_public_key
smart channel --add localRepo type=rpm-dir name=LocalRPMs manual=true priority=1 path=/root/rpm/
smart update
smart upgrade
smart install upm